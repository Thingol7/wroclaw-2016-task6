System.register(['angular2/platform/browser', './GangOfFourTask6'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var browser_1, GangOfFourTask6_1;
    return {
        setters:[
            function (browser_1_1) {
                browser_1 = browser_1_1;
            },
            function (GangOfFourTask6_1_1) {
                GangOfFourTask6_1 = GangOfFourTask6_1_1;
            }],
        execute: function() {
            browser_1.bootstrap(GangOfFourTask6_1.GangOfFourTask6);
        }
    }
});
