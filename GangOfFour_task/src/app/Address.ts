export class Address {

  type: string;
  humanReadableName: string;
  ipfsHash: string;
}
